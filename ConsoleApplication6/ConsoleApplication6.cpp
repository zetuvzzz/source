﻿#include <iostream>
#include <array>
#include <locale>

using namespace std;

class Animal {


public:

	Animal() { cout << "Animal()" << endl; }
	virtual ~Animal() { cout << "~Animal()" << endl; }

	virtual void Voice()
	{
		cout << "text \n";
	}

};
class Cat : public Animal {

public:

	void Voice() override
	{
		cout << "Mau \n";
	}
};

class Dog : public Animal
{

public:

    void Voice() override
    {
	 	cout << "Woof \n";
	}
 
};

class Spider : public Animal {

public:

	void Voice() override
	{
		cout << "Hhh \n";
	}

};

int main() {

	Animal* array[3];
	array[0] = new Dog;
	array[1] = new Cat;
	array[2] = new Spider;

	cout << "==============================" << "\n";

	for (int i = 0; i < 3; i++)
	{
	   array[i]->Voice();
	   delete array[i];
	}

}