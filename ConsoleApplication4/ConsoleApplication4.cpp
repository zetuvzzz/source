﻿
#include <cmath>
#include <iostream>

class Example
{
private:
	int a;
public:
	int GetA()
	{                                           
		return a;
	}
	           
	void SetA(int newA)
	{
		a = newA;
	}
};



class Vector
{
public:
	Vector() : x(5), y(5), z(5)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double Show()
	{
		double result = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << '\n' << x << ' ' << y << ' ' << z;
		return result;
	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Example temp, tempI;
	temp.SetA(5);
	tempI.SetA(10);
	std::cout << temp.GetA() << ' ' << tempI.GetA();

	{
		Vector v;
		v.Show();
	}

}